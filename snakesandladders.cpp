/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: Trevor
 *
 * Created on April 21, 2016, 4:58 PM
 */

/*******************************************************
 *                  SnakeAndLadder.cpp
 *  This is snake and ladder game program. This program
 *  will display the snakes and ladder board.
 *
 *
 *
 * Created and modified by :
 *  1. Chaikrit Ouisui 14255307
 *  2. Thitivuth Rattanasriampaipong 14254811
 *  3. Trevor King 14187293
 *
 *
 ******************************************************/

#include <iostream>
#include <cstdlib>
#include <cstdio>
#include<time.h>



using namespace std;



/* player class */
class player{
    private:
        string name;
        int currentPosition;
    
    public:
   
        int diceRoll(); //function to roll the dice
        void setName(string name);
        string getName();
        void setCurrentPosition(int space);
        void setNewPosition(int space);
        int getCurrentPosition();
        int isSnake(int space);
        int isLadder(int space);
        player(string name);
        
        
};

/* constructor implementation */
player :: player(string name){
    currentPosition = 1;
    this->name = name;
}
/* function to control the dice. The dice can only show 1 - 6. */
int player :: diceRoll(){
    int value = (rand()%(6)+1);
    cout << "You rolled a: " << value << endl;
    return value;
}

void player :: setName(string name){
    this->name = name;
}

string player ::getName(){
    return this->name;
}

void player::setCurrentPosition(int space){
    this->currentPosition = space;
}

void player::setNewPosition(int space){
    this->currentPosition += space;
}

int player::getCurrentPosition(){
    return this->currentPosition;
}

int player::isLadder(int space){
    switch (space){
        case 5:
            cout << "You hit a Ladder at space 5!!!" << endl;
            return 16;
        case 12:
            cout << "You hit a Ladder at space 12!!!" << endl;
            return 29;
        case 33:
            cout << "You hit a Ladder at space 33!!!" << endl;
            return 52;
        case 62:
            cout << "You hit a Ladder at space 62!!!" << endl;
            return 77;
        case 84:
            cout << "You hit a Ladder at space 84!!!" << endl;
            return 97;
    }
    
    return space;
}

int player::isSnake(int space){
    switch (space){
        case 22:
            cout << "You hit a Snake at space 22..." << endl;
            return 7;
        case 45:
            cout << "You hit a Snake at space 45..." << endl;
            return 27;
        case 58:
            cout << "You hit a Snake at space 58..." << endl;
            return 42;
        case 75:
            cout << "You hit a Snake at space 75..." << endl;
            return 61;
        case 95:
            cout << "You hit a Snake at space 95..." << endl;
            return 81;
    }
    
    return space;
}



/* main class */
int main(){
    srand(time(0));
    
    string playerName1, playerName2;
    
    cout << "What would you like to name the first player?: ";
    cin >> playerName1;
    
    cout << "What would you like to name the second player?: ";
    cin >> playerName2;
    
    cin.get();

    player player1 = player(playerName1);
    player player2 = player(playerName2);   
    
    
    
    
    while(player1.getCurrentPosition() < 100 && player2.getCurrentPosition() < 100){
        
        cout << player1.getName() << " press a key to roll!";
        cin.get();
        player1.setNewPosition(player1.diceRoll());
        
        player1.setCurrentPosition(player1.isLadder(player1.getCurrentPosition()));
        player1.setCurrentPosition(player1.isSnake(player1.getCurrentPosition()));
#
        cout << player1.getName() << " is currently at: " << player1.getCurrentPosition() << endl;
        
        cout << player2.getName() << " press a key to roll!";
        cin.get();
        player2.setNewPosition(player2.diceRoll());
        
        player2.setCurrentPosition(player2.isLadder(player2.getCurrentPosition()));
        player2.setCurrentPosition(player2.isSnake(player2.getCurrentPosition()));

        cout << player2.getName() << " is currently at: " << player2.getCurrentPosition() << endl;
    }
    
    if(player1.getCurrentPosition() >= 100 && player2.getCurrentPosition() < player1.getCurrentPosition()){
        cout << "Congratulations " << player1.getName() << " you are the winner!" << endl;
    }else{
        cout << "Congratulations " << player2.getName() << " you are the winner!" << endl;
    }
    
}

